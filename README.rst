##################
Modulaire functies
##################

Workshop voor de RWS Datalab Academy

Beginnen
--------
Maak een virtual environment op je favoriete manier, bijvoorbeeld:

.. code-block:: console

    python3 -m virtualenv <naam_van_je_env>

Installeer de benodigde packages:

.. code-block:: console

    pip install -r requirements.txt

De workshop
-----------
Run de code in het notebook. Als dit niet werkt moeten we dat eerst oplossen.

Als je notebook werkt: open run_model.py en vul de functies in zodat ze hetzelfde doen als het notebook.

Opdelen in meer functies is ook prima! En als je andere functionaliteit wil toevoegen: alleen maar leuk.

De data staat nu in het mapje "data". Kun je dit flexibel maken zodat we het aan kunnen passen naar een andere locatie, zonder dat we de code zelf hoeven te veranderen?

Als je dit werkend hebt, pas het dan aan voor een model naar keuze. Heb je zelf nog nooit een model getraind, dan kan je bijvoorbeeld `het Keras tutorial <https://www.tensorflow.org/tutorials/keras/classification>`_ volgen.

Maak een branch voor jezelf en upload je code naar de repo.


License
*******

| Copyright (c) 2020-2022, Rijkswaterstaat
|
| All Rights Reserved.
| Unauthorized copying of rws-replicate software, via any medium is strictly prohibited.
| Proprietary and confidential.
