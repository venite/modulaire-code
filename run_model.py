# -*- coding: utf-8 -*-

"""Main script for running a model."""


def load_model():
    """
    Loads the model into memory.

    Returns
    -------
    Keras-compliant model
    """
    pass


def load_images():
    pass


def run_model_on_data():
    model = load_model()
    images = load_images()
    print(model.predict(images))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    run_model_on_data()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
